#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/errno.h>
#include <string.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctype.h>


#define MAXNAME 1024

extern int errno;

int main(int argc, char ** argv){
        int socket_fd;   
        int length; 
        int nbytes; 
        char buf[BUFSIZ];
        struct sockaddr_in myaddr; 
        struct sockaddr_in client_addr; 
        int portno;

        if (argc !=  3) {
            fprintf (stderr,"usage: %s <port> <file name>\n", argv[0]);
            exit(1);
        }

        /* check port number */
        portno = atoi(argv[1]);
        if ((portno <  0) || (portno > 65535)) {
            fprintf (stderr, "%s: Illegal port number, exiting!\n", argv[0]);
            exit(1);
        }

        FILE* fp;
        fp = fopen(argv[2], "w");
        
        if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) <0) {
                perror ("socket failed");
                exit(EXIT_FAILURE);
        }

        bzero ((char *)&myaddr, sizeof(myaddr));
        myaddr.sin_family = AF_INET;
        myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        myaddr.sin_port = htons(portno);

        if (bind(socket_fd, (struct sockaddr *)&myaddr, sizeof(myaddr)) <0) {
                perror ("bind failed\n");
                exit(1);
        }

        length = sizeof(client_addr);
        //printf("Server is ready to receive !!\n");
        while (1) {
            if ((nbytes = recvfrom(socket_fd, &buf, MAXNAME, 0, (struct sockaddr*)&client_addr, (socklen_t *)&length)) <0) {
                    perror ("could not read datagram!!");
                    continue;
            }
//            printf("%d %s \n", line_num, buf);

            fputs(buf, fp);

                /* return to client */
            if (sendto(socket_fd, &buf, nbytes, 0, (struct sockaddr*)&client_addr, length) < 0) {
                    perror("Could not send datagram!!\n");
                    continue;
            }
            fclose(fp);
        }
        close(socket_fd);
}
