#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <string.h>

#include <ctype.h>
#include <unistd.h>

#define MAXDATA   1024

int main(int argc, char **argv){
        int fd;     
        int size;    
        char data[MAXDATA];
        char data1[MAXDATA];  
        struct hostent *hp;   
        struct sockaddr_in myaddr;   
        struct sockaddr_in servaddr;
        char tmp_buf[MAXDATA];
        int a[8]={0,1,2,3,4,5,6,7};
        int a_len=0;
        /*
         * Check for proper usage.
         */
        if (argc < 3) {
                fprintf (stderr, "Usage: %s host_name(IP address) file_name\n", argv[0]);
                exit(2);
        }

        if ((fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
                perror ("socket failed!");
                exit(1);
        }

        bzero((char *)&myaddr, sizeof(myaddr));
        myaddr.sin_family = AF_INET;
        myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
        myaddr.sin_port = htons(0);

        if (bind(fd, (struct sockaddr *)&myaddr,
                                sizeof(myaddr)) <0) {
                perror("bind failed!");
                exit(1);
        }

        bzero((char *)&servaddr, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_port = htons(atoi(argv[2]));
        hp = gethostbyname(argv[1]);

        if (hp == 0) {
                fprintf(stderr, "could not obtain address of %s\n", argv[2]);
                return (-1);
        }
        bcopy(hp->h_addr_list[0], (caddr_t)&servaddr.sin_addr, hp->h_length);

        while (fgets(tmp_buf, 100, stdin)) {
            //printf("a = %d , len = %d \n", a[0], strlen(data));
            data[strlen(data)] = a[a_len]+'0';
            data[strlen(data)] = 32;
            //printf("data = %s \n", data);
            strcat(data, tmp_buf);
            a_len++;
        }
        //printf("data = %s \n", &data[0]);
        data[strlen(data) + 1] = '\0';
        /* Data to Server */
        size = sizeof(servaddr);
        if (sendto(fd, data, strlen(data), 0, (struct sockaddr*)&servaddr, size) == -1) {
                perror("write to server error !");
                exit(1);
        }
        printf("data1 = %s\n", data1);
}
