/**
 *  Source file for a linked list in C
 *
 * @authors:   		Michael Denzel
 * @creation_date:	2016-09-05
 * @contact:		m.denzel@cs.bham.ac.uk
 */

//standard includes

// TODO: Add the includes you need

//own includes
#include "linkedlist.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
// TODO: Implement those methods!
int get(list * l, unsigned int index){
	list *cu_list = l;
	int count = 0;
	while(cu_list != NULL) {
	//	printf(" %d \n", cu_list->num);
		if (count == index)
			return(cu_list->num);
		count++;
		cu_list = cu_list->next;
	}
	assert(0);
}

int prepend(list * l, int data){
	l->num = data;
	return 0;
}

int append(list * l, int data){
	if( l == NULL) {
		printf("NULL");
		return 1;
	}
	list *tmp, *p = l;
        tmp = (list *) malloc(sizeof(list));
	tmp->num = data;
	p->next = tmp;
	tmp->next = NULL;
	return 0;
}

int remove_element(list * l, unsigned int index){
	list *cu = l;
	list *pre_node = cu;
	list *functionList = (struct list*) l;
        int count = 0;
	
	if(cu->next == NULL && index == 0) {
		functionList->num=0; 
		free(functionList->next);
		functionList=NULL;
		printf("empty list\n");
		return -1;
        }

        while(cu->next != NULL) {
		if (index == 0) {
			cu = cu->next;
			pre_node->num = cu->num;
			pre_node->next = cu->next;
			free(cu);
			break;
		} else {
                	if (count == index){
				pre_node->next = cu->next;
				free(cu);
				break;
			} else {
				pre_node = cu;
				cu = cu->next;
			}
		}
                count++;
        }
	if(cu == 0) {
		printf("empty list \n");
	}
	return 0;
}

int insert(list * l, unsigned int index, int data){
	list *tmp, *cu = l;
	int count=0;
	tmp = (list *) malloc(sizeof(list));
	tmp->num = data;

	int num=0;
	while(l!=NULL){
		num++;
		l=l->next;
	}
	if(index>num) return -1;	

	while(cu->next != 0) {
		if(count==index) {
			tmp->next = cu->next;
			break;
		} else {
			cu = cu->next;
		}
	count++;
	}
	cu->next = tmp;
	return 0;

}

void print_list(list * l){
	list *cu_list = l;
        while(cu_list != NULL) {
              printf("%d ", cu_list->num);
		cu_list = cu_list->next;
        }
	printf("\n");

}

void init(list * l){
	l->next=NULL;
}

void destroy(list *l){
	list *cu = l;
	list *next;
	while(cu != NULL){
		next = cu->next;
		free(cu->next);
		cu = next;
	}
	l=NULL;
	printf("empty list");
}

