#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <errno.h>
 
int64_t my_getline(char **restrict line, size_t *restrict len, FILE *restrict fp) {
     
     char chunk[128];
 
     if(*line == NULL || *len < sizeof(chunk)) {
         *len = sizeof(chunk);
         if((*line = malloc(*len)) == NULL) {
             errno = ENOMEM;
             return -1;
         }
     }
 
     (*line)[0] = '\0';
 
     while(fgets(chunk, sizeof(chunk), fp) != NULL) {
         size_t len_used = strlen(*line);
         size_t chunk_used = strlen(chunk);
 
         if(*len - len_used < chunk_used) {
             if(*len > SIZE_MAX / 2) {
                 errno = EOVERFLOW;
                 return -1;
             } else {
                 *len *= 2;
             }
             
             if((*line = realloc(*line, *len)) == NULL) {
                 errno = ENOMEM;
                 return -1;
             }
         }
 
         memcpy(*line + len_used, chunk, chunk_used);
         len_used += chunk_used;
         (*line)[len_used] = '\0';
 
         if((*line)[len_used - 1] == '\n') {
             return len_used;
         }
     }
 
     return -1;
}

int main(int argc, char ** argv) {
        // TODO: Implement your code here!

	char *line = NULL;
	size_t len = 0;
	int i=0,q,w;
        char a[5000][1000]={{0}}, *temp=malloc(1000);

     	while(my_getline(&line, &len, stdin) != -1) {
		strcpy(a[i], line);
		for(w=i;w>0;w--) { 
			for(q = w-1;q>=0;q--) {
				if(strcmp(a[q],a[w])>0) { 
                    			strcpy(temp,a[q]); 
                    			strcpy(a[q],a[w]); 
                    			strcpy(a[w],temp); 
                		} 
        		} 	
		}
		i++;
	}
 
        for(q=0; q<i; q++) {
                printf("%s", a[q]);
         //       if(q!=(i-1)) printf("\n");
        }

	free(line);

	return 0;
}
	
