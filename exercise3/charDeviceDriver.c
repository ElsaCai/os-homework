#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/spinlock.h>
#include <linux/fs.h> 
#include <linux/semaphore.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include "charDeviceDriver.h" //ioctl header file

DEFINE_MUTEX(rw_lock);
static int Major;
static size_t max_msg_len = 4 * 1024 * sizeof(char);
static size_t max_msg_ls_len = 2 * (1024 * 1024) * sizeof(char);
static long ioctl_funcs(struct file *, unsigned int , unsigned long );
static ssize_t read(struct file *, char *, size_t, loff_t *);
static ssize_t write(struct file *, const char *, size_t , loff_t *);
int append(struct list*, char*, size_t);
int remove(struct list*, char**);
int destroy(struct list*);

int append(struct list *ls, char *buf, size_t len)
{
	struct node *cur = ls->head;
	if (!cur) {
		ls->head = kmalloc(sizeof(struct node), GFP_KERNEL);
		if (!ls->head)
			return 0;

		ls->head->msg = kmalloc(sizeof(char) * len, GFP_KERNEL);
		strcpy(ls->head->msg, buf);
		ls->head->next = NULL;
	} else {
		while (cur->next)
			cur = cur->next;

		cur->next = kmalloc(sizeof(struct node), GFP_KERNEL);
		if (!cur->next)
			return 0;
		cur->next->msg = kmalloc(sizeof(char) * len, GFP_KERNEL);
		strcpy(cur->next->msg, buf);
		cur->next->next = NULL;
	}

	// Update total size
	ls->ls_msg_size += len * sizeof(char);

	return 1;
}

int remove(struct list *ls, char **buf)
{
	struct node *next_node = NULL;
	int len;
	size_t msg_sz;

	if (ls->head == NULL)
		return 0;

	next_node = ls->head->next;

	len = strlen(ls->head->msg) + 1;
	msg_sz = len * sizeof(char);

	*buf = kmalloc(msg_sz, GFP_KERNEL);
	strcpy(*buf, ls->head->msg);

	ls->ls_msg_size -= msg_sz;

	kfree(ls->head->msg);
	kfree(ls->head);
	ls->head = next_node;

	return 1;
}

int destroy(struct list *ls)
{
	struct node *cur = ls->head;
	struct node *tmp;

	while (cur != NULL) {
		tmp = cur;
		cur = cur->next;
		kfree(tmp);
	}

	ls->head = NULL;
	ls->ls_msg_size = 0;

	return 1;
}

int open(struct inode *inode, struct file *filp)
{

    printk(KERN_INFO "Inside open \n");
    return 0;
}

int release(struct inode *inode, struct file *filp)
{
    printk(KERN_INFO "Inside close \n");
    return 0;
}

static ssize_t read(struct file *filp, char *buff, size_t count, loff_t *offp) 
{
    int bytes_read = 0;
	char *ptr;
	printk("Hello read \n");	

	mutex_lock(&rw_lock);
	if (!remove(&msg_ls, &msg_ptr)) {
		mutex_unlock(&rw_lock);
		return -EAGAIN;
	}

	printk("Read string: %s\n", msg_ptr);
	printk("MAX message size: %zu\n",  max_msg_len);
	printk("List message size: %ld\n", msg_ls.ls_msg_size);
	printk("MAX messages size: %zu\n\n", max_msg_ls_len);

	if (*msg_ptr == 0) {
		mutex_unlock(&rw_lock);
		return 0;
	}

	ptr = msg_ptr;

	while (count && *ptr) {
		put_user(*(ptr++), buff++);

		count--;
		bytes_read++;
	}

	if (*ptr == '\0')
		put_user(*ptr, buff);

	kfree(msg_ptr);
	msg_ptr = NULL;
	mutex_unlock(&rw_lock);

	return bytes_read;
}

static ssize_t write(struct file *file, const char *buf, size_t count, loff_t *ppos)
{
    int len_nb = count + 1;
	size_t msg_size = len_nb * sizeof(char);
	char *msg_buf;
    printk("Hello write \n");

	printk("Message size: %zd\n", msg_size);
	printk("MAX message size: %zu\n", max_msg_len);
	printk("List message size: %ld\n", msg_ls.ls_msg_size);
	printk("MAX messages size: %zu\n\n", max_msg_ls_len);

	if (msg_size > max_msg_len) {
		printk(KERN_ALERT "Error: the message size is bigger than 4K");
		return -EINVAL;
	}

	if (msg_ls.ls_msg_size + msg_size > max_msg_ls_len) {
		printk(KERN_ALERT "Error: the messages size is over than the maximum");
		return -EAGAIN;
	}

	mutex_lock(&rw_lock);
	msg_buf = kmalloc(sizeof(char) * len_nb, GFP_KERNEL);
	strncpy_from_user(msg_buf, buf, len_nb - 1);
	msg_buf[len_nb - 1] = '\0';

	if (!append(&msg_ls, msg_buf, len_nb))
		printk(KERN_ALERT "Error: from append the message");

	kfree(msg_buf);
	mutex_unlock(&rw_lock);

	return count;
}

static long ioctl_funcs(struct file *filp, unsigned int cmd, unsigned long arg)
{
    int ret = 0;
   /*switch (cmd)
    {
        case IOCTL_HELLO:
            printk(KERN_INFO "Hello ioctl world");
            break;
    }*/
    int msg_max = (int)arg;

	if (msg_max > max_msg_ls_len || msg_max > msg_ls.ls_msg_size) {
		printk("New max len: %d\n\n", msg_max);
		max_msg_ls_len = msg_max;
		return ret;
	}

	//printk("Ma nooooo\n");

	return -EINVAL;
}

struct file_operations fops = {
    .open = open,
    .unlocked_ioctl = ioctl_funcs,
    .release = release,
    .read = read,
    .write = write
};

struct cdev *kernel_cdev;

int char_arr_init(void)
{
    int ret;
    dev_t dev_no, dev;

    kernel_cdev = cdev_alloc();
    kernel_cdev->ops = &fops;
    kernel_cdev->owner = THIS_MODULE;
    printk(" Inside init module\n");
    ret = alloc_chrdev_region(&dev_no, 0, 1, "char_arr_dev");
    if (ret < 0)
    {
        printk("Major number allocation is failed\n");
        return ret;
    }

    Major = MAJOR(dev_no);
    dev = MKDEV(Major, 0);
    printk(" The major number for your device is %d\n", Major);
    printk("'mknod /dev/opsysmem c %d 0'.\n", Major);
    ret = cdev_add(kernel_cdev, dev, 1);
    if (ret < 0)
    {
        printk(KERN_INFO "Unable to allocate cdev");
        return ret;
    }

    msg_ls.head = NULL;
	msg_ls.ls_msg_size = 0;
    return 0;
}

void char_arr_cleanup(void)
{
    //printk(KERN_INFO " Inside cleanup_module\n");
    cdev_del(kernel_cdev);
    unregister_chrdev_region(Major, 1);
	destroy(&msg_ls);
}
MODULE_LICENSE("GPL");
module_init(char_arr_init);
module_exit(char_arr_cleanup);
