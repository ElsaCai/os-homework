#include <linux/ioctl.h>
#define IOC_MAGIC 'k'
#define IOCTL_HELLO _IO(IOC_MAGIC,0)

struct node {
	char *msg;
	struct node *next;
};

struct list {
	struct node *head;
	size_t ls_msg_size;
};

static struct list msg_ls;
static char *msg_ptr;